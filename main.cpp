#include <vector>
#include <list>

#include <math.h>

#include "STLfunctions.h"
#include "CSTLGroup.h"
#include "CPath.h"

using namespace std;


int main(int argc, char** argv)
{
    unsigned int i;
    int j, NumPecas;
    bool bFileNameExist, bArquivoCarregado, bDepura; // bProjecao;
    unsigned int nTriangles;
    v3 tempVertex;

    string strCurArgv;
    vector<tri> ReadTriFile;
    list<tri> AlingTriangle;
    CSTLGroup grupoPecas;
    CSTLPathGroup grupoProjecao;
//    vector<v3> vecOut;

  //  list<CAdjacentsTriangles> vecAdjTriangles;

    bArquivoCarregado = false;
    bDepura = false;
    cout << "Início da lista" << endl;
    for (j = 0; j < argc; j++)
    {
        strCurArgv = argv[j];
        cout << argv[j] << endl;
        bDepura = bDepura || (strCurArgv.compare("-d") == 0);
        if (strCurArgv.compare("-f") == 0)
        {
            bFileNameExist = ((j + 1) <= argc);

            if (bFileNameExist)
            {
                cout << "Carregando arquivo " << argv[j+1] << endl;
                bArquivoCarregado = (read_stl(argv[j+1], ReadTriFile) == 0);
            }
            else
                cout << "Arquivo inválido. Entre com um nome de arquivo após o -f" << endl;

        }
    }

    if (bArquivoCarregado && bDepura)
    {

        nTriangles = ReadTriFile.size();
        cout << "Numero de faces " << nTriangles << endl;

        // Alinha a peça com a origem
        AlingTriangle = AlingWith(ReadTriFile,v3(0.0,0.0,0.0));
        // Separa o modelo em diferentes peças
        NumPecas = grupoPecas.Process_Triangles(AlingTriangle);

        /*
        cout << "Numero de peças " << NumPecas << endl;
        for (i = 0; i < (unsigned int)NumPecas; i++)
            cout << "Peça " << i << " com " << grupoPecas.vecAdjTriGroup[i].numConectionsAdjacentList() << " vinculos." << endl;

        cout << "Tringulos nas peças " << grupoPecas.Count_Triangles() << endl;
        cout << "Numero de peças " << grupoPecas.vecAdjTriGroup.size() << endl;
        grupoPecas.PrintIdxTriangleListAdj("Lista_triangulos.txt");
        */

        // realiza a projeção em um valor determinado de Z
        grupoPecas.ProjectZ(-1.0);
        //grupoPecas.PrintRawPathGroup("Teste_Pontos.txt");
        grupoPecas.PrintPathGroup("pontoZ.txt");

        grupoPecas.BuildInitialRoute();



    }
    cout << "Fim do arquivo" << endl;
    return 0;
}
