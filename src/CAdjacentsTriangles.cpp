#include "CAdjacentsTriangles.h"

//---------------------------------------------------------------------
//
int CAdjacentsTriangles::AddAdjacent(list<tri> &vecT, vector<int> &vOriIndex)
{
    unsigned int i, j;
    int numVertex, numIndex;
    tri currTriangle, testTriangle, auxTri;

    list<tri> vecAux;
    list<tri> vecRes;
    vector<int> vOriIndexAux;

    vecAux = vecT;
    vOriIndexAux = vOriIndex;
    this->vecTriangles.clear();
//    this->vecOriginalIndex.clear();


    list<tri>::iterator refVecAux, refFim, refMove;
    vector<int>::iterator idxMove;
    //nPathsvector<tri>::iterator refVectorInicio, refVectorFim;

    refFim = vecAux.end();
    refVecAux = vecAux.begin();
    idxMove = vOriIndexAux.begin();
    auxTri = *refVecAux;
    numIndex = *idxMove;
    auxTri.triIndex = numIndex;
    this->vecTriangles.push_back(auxTri);
    //this->vecOriginalIndex.push_back(numIndex);

    refVecAux = vecAux.erase(refVecAux);
    idxMove = vOriIndexAux.erase(idxMove);

    i = 0;
    while(i < this->vecTriangles.size())
    {
        refMove = refVecAux;
        currTriangle = this->vecTriangles[i];
        j = 0;
        while(refMove != refFim)
        {
            auxTri = *refMove;
            numIndex = *idxMove;

            numVertex = currTriangle.NumCompVertex(auxTri);
            if (numVertex > 0)
            {
                auxTri.triIndex = numIndex;
                this->vecTriangles.push_back(auxTri);
                //this->vecOriginalIndex.push_back(numIndex);
                refMove = vecAux.erase(refMove);
                idxMove = vOriIndexAux.erase(idxMove);
            }
            else
            {
                ++idxMove;
                ++refMove;
            }

            j++;
        }
        refVecAux = vecAux.begin();
        idxMove = vOriIndexAux.begin();
        i++;
    };
    vecT = vecAux;
    vOriIndex = vOriIndexAux;
    return (int)vecTriangles.size();
};
//---------------------------------------------------------------------
bool CAdjacentsTriangles::BuilAdjacentList()
{
    unsigned int i, j, numTriangles;
    tri currTriangle;
    indexList tempList;
    numTriangles = vecTriangles.size();

    //vecAdjacents.clear();
    for(i = 0; i < numTriangles; i++)
    {
        currTriangle = vecTriangles[i];
        tempList.clear();
        for(j = 0; j < numTriangles; j++)
        {
            if (currTriangle.NumCompVertex(vecTriangles[j]) == 2)
                tempList.push_back(vecTriangles[j].triIndex);
        }
        currTriangle.AdjIndex = tempList;
        vecTriangles[i] = currTriangle;
        //vecAdjacents.push_back(tempList);
    }
    return true; //(vecAdjacents.size() == vecTriangles.size());
};
//---------------------------------------------------------------------
int CAdjacentsTriangles::numConectionsAdjacentList()
{
    unsigned int i, numTriangles, numConections;

    numTriangles = vecTriangles.size();
    numConections = 0;
    for(i = 0; i < numTriangles; i++)
        numConections += vecTriangles[i].AdjIndex.size();

    return (int)numConections;
};
//---------------------------------------------------------------------
//=====================================================================
//---------------------------------------------------------------------
bool CAdjacentsTriangles::AnaliseProjectZ(double z)
{
    unsigned int i, j, k, nTriangles, nVertex, idx0, idx1;
    double dotProd, tFactor;
    v3 tempPoint0, tempPoint1, projPoint;
    tri tempTriangle;
    CAdjacentsTriangles tempPiece;
    indexList tempListIndex;
    pointList tempListaPontos;
    v3 vecL1, vecL2, vecNorm, PointXY(1,1,0), PointZ(0,0,1);
    PointGroup tempPointGroup;

    nTriangles = this->vecTriangles.size();
    if (nTriangles < 1)
        return false;

    vecPointsOnZplane.clear();
    for (k = 0; k < nTriangles; k++) // Loop nos triangulos de cada peças
    {
        nVertex = 0;
        tempTriangle = this->vecTriangles[k];
        vecNorm = tempTriangle.u_v;
        vecNorm.m_z = z;
        vecNorm = vecNorm/vecNorm.mod();
        tempPointGroup = PointGroup();
        tempPointGroup.triIndex = tempTriangle.triIndex;
        tempListaPontos.clear();
        if ( (tempTriangle.m_p[0].m_z == z) &&
             (tempTriangle.m_p[1].m_z == z) &&
             (tempTriangle.m_p[2].m_z == z) )
        {
            // Não estou ordenando estes pontos. Isso pode dar um problema no futuro
            for(i = 0; i < nVertex; i++)
            {
                tempListaPontos.push_back(tempTriangle.m_p[i]);
            }

        }
        else
        {
            vecL1 = tempTriangle.m_p[1] - tempTriangle.m_p[0];
            vecL2 = tempTriangle.m_p[2] - tempTriangle.m_p[1];
            dotProd = PointXY.dot_prod(vecL1.cross_prod(vecL2));

            if (!(dotProd == 0))
            {
                for (j = 0; j < 3; j++) // Loop nos pontos dos triangulos
                {
                    idx0 = j;
                    idx1 = (j+1) % 3;

                    tempPoint0 = tempTriangle.m_p[idx0];
                    tempPoint1 = tempTriangle.m_p[idx1];
                    if ((tempPoint0.m_z - z)*(tempPoint1.m_z - z) < 0)
                    {
                        tFactor = (z - tempPoint0.m_z)/(tempPoint1.m_z-tempPoint0.m_z);
                        projPoint.m_x = (tempPoint1.m_x-tempPoint0.m_x)*tFactor + tempPoint0.m_x;
                        projPoint.m_y = (tempPoint1.m_y-tempPoint0.m_y)*tFactor + tempPoint0.m_y;
                        projPoint.m_z = z;
                        tempListaPontos.push_back(projPoint);
                     //   nVertex++;
                    }
                }
            }
            vecL1 = tempListaPontos[0] + tempListaPontos[1];
            vecNorm = vecNorm*vecL1.mod() + (tempListaPontos[0] + tempListaPontos[1])*0.5;
            AdjustCCWOrderWithPoint(tempListaPontos,vecNorm);

        }

        tempPointGroup.vecPoints = tempListaPontos;
        this->vecPointsOnZplane.push_back(tempPointGroup);
    }
    Zprojection = true;
    Zplane = z;
    return Zprojection;
};
//---------------------------------------------------------------------
// --- Anota os triangulos vinculados no corte por Z
bool CAdjacentsTriangles::BuildLinksProjectZ()
{
    unsigned int i, j, k, nTriOnZPlane, nTriangles, nVertex, idx0, nPointsB; // nCurrIndex
    int tempIndex;
    indexList tempList, currList;
    pointList tempPointList;
    tri tempTriangle;
    PointGroup tempPointGroup, tempPointGroupB;

    nTriOnZPlane = this->vecPointsOnZplane.size();
    nTriangles = this->vecTriangles.size();
    /*
    for (k = 0; k < nTriangles; k++) // Loop nos triangulos de cada peças
    {
        tempTriangle = this->vecTriangles[k];
        currList = tempTriangle.AdjIndex;
        cout << "T\t" << tempTriangle.triIndex << "\t";
        for (j = 0; j < 3; j++)
            cout << tempTriangle.m_p[j] << "\t";
        for (j = 0; j < currList.size(); j++)
            cout << currList[j] << "\t";
        cout << endl;
        tempPointGroup = this->vecPointsOnZplane[k];
        cout << "G\t" << tempPointGroup.triIndex << "\t";
        for (j = 0; j < tempPointGroup.vecPoints.size(); j++)
            cout << tempPointGroup.vecPoints[j] << "\t";

        for (j = 0; j < tempPointGroup.AdjIndex.size(); j++)
            cout << tempPointGroup.AdjIndex[j] << "\t";
        cout << endl;
    }
*/
    for (k = 0; k < nTriangles; k++) // Loop nos triangulos de cada peças
    {
        tempTriangle = this->vecTriangles[k];
        currList = tempTriangle.AdjIndex;

        tempPointGroup = this->vecPointsOnZplane[k];

        tempPointList.clear();
        tempPointList.push_back(MeanPoint(tempPointGroup.vecPoints));
        if (tempPointGroup.vecPoints.size() > 0)
        {
            //nCurrIndex = this->vecTriangles[k].triIndex;

            nVertex = currList.size();
            tempList.clear();

            for (j = 0; j < nVertex; j++)
            {
                idx0 = currList[j];
                for (i = 0; i < nTriOnZPlane; i++)
                {
                    tempPointGroupB = this->vecPointsOnZplane[i];
                    if ((int)idx0 == tempPointGroupB.triIndex)
                        break;
                }
                nPointsB = tempPointGroupB.vecPoints.size();
                if(nPointsB > 0)
                {
                    tempList.push_back(idx0);
                    tempPointList.push_back(MeanPoint(tempPointGroupB.vecPoints));
                }

            }

            /*
            if (!IsOnCCWOrder(tempPointList))
            {
                tempIndex = tempList[1];
                tempList[1] = tempList[0];
                tempList[0] = tempIndex;
            }
            */
            tempPointGroup.AdjIndex = tempList;
            this->vecPointsOnZplane[k] = tempPointGroup;
        }

    }
    // Verbose pós

    for (k = 0; k < nTriangles; k++) // Loop nos triangulos de cada peças
    {
        tempTriangle = this->vecTriangles[k];
        currList = tempTriangle.AdjIndex;
        cout << "T\t" << tempTriangle.triIndex << "\t";
        for (j = 0; j < 3; j++)
            cout << tempTriangle.m_p[j] << "\t";
        for (j = 0; j < currList.size(); j++)
            cout << currList[j] << "\t";
        cout << endl;
        tempPointGroup = this->vecPointsOnZplane[k];
        cout << "G\t" << tempPointGroup.triIndex << "\t";
        for (j = 0; j < tempPointGroup.vecPoints.size(); j++)
            cout << tempPointGroup.vecPoints[j] << "\t";

        for (j = 0; j < tempPointGroup.AdjIndex.size(); j++)
            cout << tempPointGroup.AdjIndex[j] << "\t";
        cout << endl;
    }


    return true;
};
//---------------------------------------------------------------------
int CAdjacentsTriangles::getNumPointsOnZplane()
{
    unsigned int k, nTriangles;
    int retNumPoints = 0;

    nTriangles = vecPointsOnZplane.size();
    if (nTriangles < 1)
        return 0;

    for (k = 0; k < nTriangles; k++) // Loop nos triangulos de cada peças
    {
        retNumPoints += vecPointsOnZplane[k].vecPoints.size();
    }
    return retNumPoints;
};
//---------------------------------------------------------------------
int CAdjacentsTriangles::ProcessPaths()
{
    unsigned int i, nPointsOnPlane, nTestSize;
    CPath tempPath;
    PointGroup tempPointList;
    indexList tempAdjList;
    list<PointGroup> lstPointsOnZplane;

    nPointsOnPlane = this->vecPointsOnZplane.size();
    if (nPointsOnPlane < 1)
        return 1;

    //nTriangles = vecPointsOnZplane.size();
    grupoCaminhos.vecPaths.clear();
    //k = 0;
    lstPointsOnZplane.clear();

    for (i = 0; i < nPointsOnPlane; i++)
    {
        tempPointList = this->vecPointsOnZplane[i];
        nTestSize = tempPointList.vecPoints.size();
        if (nTestSize > 0)
        {
            lstPointsOnZplane.push_back(this->vecPointsOnZplane[i]);
        }
    }

    if (lstPointsOnZplane.size() < 1)
        return 1;

    list<PointGroup>::iterator itMove;
    tempPath = CPath();

    itMove = lstPointsOnZplane.begin();
    tempPointList = *itMove;
    tempPath.vecPointsOnZPath.push_back(tempPointList);
    itMove = lstPointsOnZplane.erase(itMove);

    do
    {
        itMove = lstPointsOnZplane.begin();
        while(itMove != lstPointsOnZplane.end()) //for (i = 0; i < lstPointsOnZplane.size(); i++)//nPointsOnList; i++) //
        {
            tempPointList = *itMove;

            if(tempPath.PointIsOnPool(tempPointList.vecPoints))
            {
                tempPath.vecPointsOnZPath.push_back(tempPointList);
                lstPointsOnZplane.erase(itMove);
                itMove = lstPointsOnZplane.begin();
            }
            else
                ++itMove;
        };

        // TODO: Ordenar tempPath.
        // tempPath.sortVecPointsOnZPathByAngle();


        grupoCaminhos.vecPaths.push_back(tempPath);

        if ((int)lstPointsOnZplane.size() > 1) //if (lstPointsOnZplane.size() > 1)
        {
            tempPath = CPath();

            itMove = lstPointsOnZplane.begin();
            tempPointList = *itMove;
            tempPath.vecPointsOnZPath.push_back(tempPointList);
            itMove = lstPointsOnZplane.erase(itMove);
        }
        else
            break;

    }while ((int)lstPointsOnZplane.size() > 0);



    return (int)grupoCaminhos.vecPaths.size();
}
//---------------------------------------------------------------------
int CAdjacentsTriangles::GetNumPaths()
{
    return (int)grupoCaminhos.vecPaths.size();
};
//---------------------------------------------------------------------
