#include "CPath.h"

v3 MeanPoint(pointList p1)
{
    unsigned int i, nP1;
    v3 v3p1 = v3(0,0,0);
    nP1 = p1.size();
    for (i = 0; i < nP1; i++)
    {
        v3p1.m_x += p1[i].m_x;
        v3p1.m_y += p1[i].m_y;
        v3p1.m_z += p1[i].m_z;
    }
    v3p1.m_x /= ((double)nP1);
    v3p1.m_y /= ((double)nP1);
    v3p1.m_z /= ((double)nP1);
    return v3p1;
}
//---------------------------------------------------------------------
bool ComparePointList(pointList p1, pointList p2)
{
    unsigned int i, j, nP1, nP2;
    bool retValue = false;
    v3 v3p1, v3p2;
    nP1 = p1.size();
    nP2 = p2.size();
    for (i = 0; i < nP1; i++)
    {
        v3p1 = p1[i];
        for (j = 0; j < nP2; j++)
        {
            v3p2 = p2[j];
            if(v3p1 == v3p2)
                return true;
        }
    }
    return retValue;
};
//---------------------------------------------------------------------
bool IsOnCCWOrder(pointList &pL1)
{
    v3 p0, p1, p2;
    p0 = pL1[0];
    p1 = pL1[1];
    p2 = pL1[2];
    double dArea = 0.5*( (p1.m_x*p2.m_y - p2.m_x*p1.m_y)
                       - (p0.m_x*p2.m_y - p2.m_x*p0.m_y)
                       + (p0.m_x*p1.m_y - p1.m_x*p0.m_y) );

    return (dArea >= 0);
};
//---------------------------------------------------------------------
void AdjustCCWOrderWithPoint(pointList &pL1, v3 p3)
{
    v3 p1, p2;
    p1 = pL1[0];
    p2 = pL1[1];
    double dArea = 0.5*( (p2.m_x*p3.m_y - p3.m_x*p2.m_y)
                       - (p1.m_x*p3.m_y - p3.m_x*p1.m_y)
                       + (p1.m_x*p2.m_y - p2.m_x*p1.m_y) );

    if (dArea > 0)
    {
        pL1[0] = p2;
        pL1[1] = p1;
    }
};
//---------------------------------------------------------------------
void AdjustCCWOrderNearPoint(pointList &pL1, v3 p4)
{
    int i;
    v3 p1, p2, p3;

    vector<double> vecDist;
    vector<int> vecIndex;
    vector<v3> vecPoint;
    for(i = 0; i < (int)pL1.size(); i++)
    {
        vecDist.push_back(p4.dist(pL1[i]));
        vecIndex.push_back(i);
        vecPoint.push_back(pL1[i]);
    }
    SortVector(vecDist,vecIndex);
    SortByVector(vecPoint,vecIndex);
    pL1.clear();
    for(i = 0; i < (int)vecPoint.size(); i++)
        pL1.push_back(vecPoint[i]);

    p1 = pL1[0];
    p2 = pL1[1];
    p3 = pL1[2];
    double dArea = 0.5*( (p2.m_x*p3.m_y - p3.m_x*p2.m_y)
                       - (p1.m_x*p3.m_y - p3.m_x*p1.m_y)
                       + (p1.m_x*p2.m_y - p2.m_x*p1.m_y) );

    if (dArea > 0)
    {
        pL1[1] = p3;
        pL1[2] = p2;
    }

};
//---------------------------------------------------------------------
double CPath::getMaxCoord(int coordN)
{
    double retMax = DBL_MIN;
    v3 tempPoint;
    list<v3> ::iterator itMove;

    for (itMove = lstPoints.begin(); itMove != lstPoints.end(); ++itMove)
    {
        tempPoint = *itMove;
        if ((coordN == 0) & (tempPoint.m_x > retMax))
        {
            retMax = tempPoint.m_x;
        }
        if ((coordN == 1) & (tempPoint.m_y > retMax))
        {
            retMax = tempPoint.m_y;
        }
        if ((coordN == 2) & (tempPoint.m_z > retMax))
        {
            retMax = tempPoint.m_z;
        }
    }
    return retMax;
};
//---------------------------------------------------------------------
double CPath::getMinCoord(int coordN)
{
    double retMin = DBL_MAX;
    v3 tempPoint;
    list<v3> ::iterator itMove;

    for (itMove = lstPoints.begin(); itMove != lstPoints.end(); ++itMove)
    {
        tempPoint = *itMove;
        if ((coordN == 0) & (tempPoint.m_x < retMin))
        {
            retMin = tempPoint.m_x;
        }
        if ((coordN == 1) & (tempPoint.m_y < retMin))
        {
            retMin = tempPoint.m_y;
        }
        if ((coordN == 2) & (tempPoint.m_z < retMin))
        {
            retMin = tempPoint.m_z;
        }
    }
    return retMin;
};
//---------------------------------------------------------------------
void CPath::OffSet(v3 vOS)
{
    v3 tempVertex = v3(0,0,0);
    list<v3> OffSetList;
    list<v3>::iterator refMove, refEnd;

    OffSetList.clear();
    refMove = lstPoints.begin();
    refEnd  = lstPoints.end();
    while (refMove != refEnd)
    {
        tempVertex = *refMove;
        tempVertex.m_x += vOS.m_x;
        tempVertex.m_x += vOS.m_x;
        tempVertex.m_x += vOS.m_x;
        OffSetList.push_back(tempVertex);
        ++refMove;
    }
    this->lstPoints = OffSetList;

};
//---------------------------------------------------------------------
PointGroup CPath::PointGroupOnPoolByIndex(int idxPoint)
{
    unsigned int i, nPoolSize;
    PointGroup retPointGroup = PointGroup();
    nPoolSize = vecPointsOnZPath.size();
    for (i = 0; i < nPoolSize; i++)
    {
        if (vecPointsOnZPath[i].triIndex == idxPoint)
        {
            retPointGroup = vecPointsOnZPath[i];
            break;
        }

    }
    return retPointGroup;
};
//---------------------------------------------------------------------
vector<double> CPath::AngleWithCenterOfPointGroupOnPoolByIndex(int idxPoint)
{
    unsigned int i, nPoolSize;
    PointGroup retPointGroup = PointGroup();
    vector<double> retAngleVector;
    double tempAngle;
    v3 v3CenterPoint, selPoint, diffVector;
    nPoolSize = vecPointsOnZPath.size();
    for (i = 0; i < nPoolSize; i++)
    {
        if (vecPointsOnZPath[i].triIndex == idxPoint)
            retPointGroup = vecPointsOnZPath[i];
    }
    v3CenterPoint = getGroupCenterPoint();
    retAngleVector.clear();
    for (i = 0; i < retPointGroup.vecPoints.size(); i++)
    {
        selPoint = retPointGroup.vecPoints[i];
        diffVector = selPoint - v3CenterPoint;
        tempAngle = diffVector.angle();
        retAngleVector.push_back(tempAngle);
    }
    return retAngleVector;
};
//---------------------------------------------------------------------
v3 CPath::getMeanPoint()
{
    double numPoints;
    v3 tempVertex, retV3 = v3(0,0,0);
    list<v3>::iterator refMove, refEnd;

    numPoints = (double)lstPoints.size();
    if (numPoints == 0)
        return retV3;

    refMove = lstPoints.begin();
    refEnd  = lstPoints.end();
    while (refMove != refEnd)
    {
        tempVertex = *refMove;
        retV3.m_x += tempVertex.m_x;
        retV3.m_y += tempVertex.m_y;
        retV3.m_z += tempVertex.m_z;
        ++refMove;
    }
    retV3.m_x /= numPoints;
    retV3.m_y /= numPoints;
    retV3.m_z /= numPoints;
    return retV3;
};
//---------------------------------------------------------------------
v3 CPath::getGroupCenterPoint()
{
    unsigned int i, j, numPoints, numElem;
    v3 tempVertex, retV3 = v3(0,0,0);


    numPoints = vecPointsOnZPath.size();
    if (numPoints == 0)
        return retV3;

    numElem = 0;
    for (i = 0; i < numPoints; i++)
    {
        for (j = 0; j < vecPointsOnZPath[i].vecPoints.size(); j++)
        {
            tempVertex = vecPointsOnZPath[i].vecPoints[j];
            retV3.m_x += tempVertex.m_x;
            retV3.m_y += tempVertex.m_y;
            retV3.m_z += tempVertex.m_z;
            numElem++;
        }
    }

    retV3.m_x /= (double)numElem;
    retV3.m_y /= (double)numElem;
    retV3.m_z /= (double)numElem;
    return retV3;
};
//---------------------------------------------------------------------
v3 CPath::getListCenterPoint()
{
    unsigned int numPoints;
    v3 retV3 = v3(0,0,0);

    list<v3>::iterator iMove;
    numPoints = lstPoints.size();
    if (numPoints == 0)
        return retV3;

    for (iMove = lstPoints.begin(); iMove != lstPoints.end(); ++iMove)
    {
        retV3.m_x += iMove->m_x;
        retV3.m_y += iMove->m_y;
        retV3.m_z += iMove->m_z;
    }

    retV3.m_x /= (double)numPoints;
    retV3.m_y /= (double)numPoints;
    retV3.m_z /= (double)numPoints;
    return retV3;
};
//---------------------------------------------------------------------
void CPath::RemoveRepeted()
{
    v3 tempTested, tempFind;
    unsigned int i, j;
    list<v3> OffSetList, tempList;
    list<v3>::iterator refMoveA, refEndA, refMoveB, refEndB;

    //cout<< "1" << endl;
    if (this->lstPoints.size() < 2)
        return;

    //cout<< "2" << endl;
    tempList = this->lstPoints;
    refMoveA = tempList.begin();
    tempTested = *refMoveA;

    OffSetList.clear();
    OffSetList.push_back(tempTested);
    refMoveA = tempList.erase(refMoveA);

    refMoveA = OffSetList.begin();
    refEndA  = OffSetList.end();
    refMoveB = tempList.begin();
    refEndB  = tempList.end();
    for(i = 0; i < OffSetList.size(); i++)
    {
        if (tempList.size() == 0)
            break;
        tempTested = *refMoveA;
        refMoveB = tempList.begin();
        refEndB  = tempList.end();
        j = 0;
//        cout <<"ini " << "(" << i << "," << OffSetList.size() << ") - " << "(" << j << "," << tempList.size() << ")" << endl;
        while((refMoveB != refEndB) && (j < tempList.size()))
        {
            tempFind = *refMoveB;
            if ((tempTested == tempFind))
                refMoveB = tempList.erase(refMoveB);
            else
                ++refMoveB;

            j++;
        }
        if (tempList.size() > 0)
        {
            refMoveB = tempList.begin();
            tempFind = *refMoveB;
            OffSetList.push_back(tempFind);
            refMoveB = tempList.erase(refMoveB);
        }

//        cout <<"fim " << "(" << i << "," << OffSetList.size() << ") - " << "(" << j << "," << tempList.size() << ")" << endl;
        ++refMoveA;

    }
    this->lstPoints = OffSetList;
};
//---------------------------------------------------------------------
void CPath::MakeCircularOrder()
{
    int i, j, m, curIndex, nPoinsOnPool;
    indexList tempCurrAdjList, curAdjList;
    PointGroup curPointGroup;
    vector<int> UsedIndex;
    bool bIndexIsUsed;
    v3 refPoint = v3(0,0,0);

    nPoinsOnPool = (int)vecPointsOnZPath.size();


    lstIndexPoint.clear();
    UsedIndex.clear();

    curPointGroup = vecPointsOnZPath[0];
    curIndex = curPointGroup.triIndex;

    for (i = 0; i < nPoinsOnPool; i++)
    {
        curPointGroup = PointGroupOnPoolByIndex(curIndex);
        curAdjList = curPointGroup.AdjIndex;

        tempCurrAdjList.clear();


        if (i == -1) // Problema com o primeiro elemento
        {
            bIndexIsUsed = false;
            for(m = 0; m < (int)UsedIndex.size(); m++)
                bIndexIsUsed = bIndexIsUsed || (UsedIndex[m] == curIndex);
            if (!bIndexIsUsed)
                tempCurrAdjList.push_back(curIndex);

            for (j = 0; j < (int)curAdjList.size(); j++)
            {
                bIndexIsUsed = false;
                for(m = 0; m < (int)UsedIndex.size(); m++)
                    bIndexIsUsed = bIndexIsUsed || (UsedIndex[m] == curAdjList[j]);

                if (!bIndexIsUsed)
                    tempCurrAdjList.push_back(curAdjList[j]);
            }
            // Solução: inverter a ordem

            int tempIndex = tempCurrAdjList[1];
            tempCurrAdjList[1] = tempCurrAdjList[0];
            tempCurrAdjList[0] = tempIndex;
            /*
            int tempIndex = tempCurrAdjList[1];
            tempCurrAdjList[1] = tempCurrAdjList[2];
            tempCurrAdjList[2] = tempIndex;
            */
        }
        else
        {

            bIndexIsUsed = false;
            for(m = 0; m < (int)UsedIndex.size(); m++)
                bIndexIsUsed = bIndexIsUsed || (UsedIndex[m] == curIndex);
            if (!bIndexIsUsed)
                tempCurrAdjList.push_back(curIndex);

            for (j = 0; j < (int)curAdjList.size(); j++)
            {
                bIndexIsUsed = false;
                for(m = 0; m < (int)UsedIndex.size(); m++)
                    bIndexIsUsed = bIndexIsUsed || (UsedIndex[m] == curAdjList[j]);

                if (!bIndexIsUsed)
                    tempCurrAdjList.push_back(curAdjList[j]);
            }

        }
        // Ordenar em CCW

        //AdjustCCWOrderNearPoint(tempCurrAdjList,refPoint);
        //refPoint =  tempCurrAdjList.back();

        for (j = 0; j < (int)tempCurrAdjList.size(); j++)
        {
            lstIndexPoint.push_back(tempCurrAdjList[j]);
            UsedIndex.push_back(tempCurrAdjList[j]);
        }

        /*
        UsedIndex.push_back(curAdjList[0]);
        UsedIndex.push_back(curAdjList[0]);
        lstIndexPoint.push_back(curIndex);
        UsedIndex.push_back(curIndex);
        UsedIndex.push_back(curAdjList[1]);
        UsedIndex.push_back(curAdjList[1]);
        */

        if (tempCurrAdjList.size() > 0)
            curIndex = tempCurrAdjList.back();
        else
            break;
    }

    /*
    list<int>::iterator itIni;
    for (itIni = lstIndexPoint.begin(); itIni != lstIndexPoint.end(); ++itIni)
        cout << "i: " << *itIni << endl;
    */

}
//---------------------------------------------------------------------
void CPath::DownToPointList()
{
    int i, m,  nPoints, curIndex;
    v3 centerPoint, middlePoint;
    //double curAngle, selAngle;
    indexList tempCurrAdjList, tempTestAdjList;
    pointList tempListOfPoints;
    PointGroup curPointGroup;
    bool bIndexIsUsed;

    vector<v3> lstTempPoints;
    vector<PointGroup> AuxVecPoints;
    vector<double> angleWithCenter;
    vector<int> idxAngleWithCenter;
    vector<int> UsedIndex;

    AuxVecPoints = this->vecPointsOnZPath;

    UsedIndex.clear();
    lstPoints.clear();

    list<int>::iterator itIni;
    for (itIni = lstIndexPoint.begin(); itIni != lstIndexPoint.end(); ++itIni)
    {
        curIndex = *itIni;
        bIndexIsUsed = false;
        for(m = 0; m < (int)UsedIndex.size(); m++)
            bIndexIsUsed = bIndexIsUsed || (UsedIndex[m] == curIndex);

        if (bIndexIsUsed)
            continue;

        curPointGroup = PointGroupOnPoolByIndex(curIndex);
        nPoints = curPointGroup.vecPoints.size();
        for (i = 0; i < nPoints; i++)
            lstPoints.push_back(curPointGroup.vecPoints[i]);

        UsedIndex.push_back(curIndex);
    }
    RemoveRepeted();
};
//---------------------------------------------------------------------
void CPath::AjustCCWList()
{
    unsigned int i, nListSize;
    v3 tempPoint;
    list<v3>::iterator iMove, iMoveNext, iMoveTemp;
    list<v3>::reverse_iterator iMoveRev;
    //list<v3> AuxList;
    vector<v3> vecAux;

    nListSize = lstPoints.size();
    v3 CenterPoint;
    bool checkCCW;
    pointList tempListPoints;

    CenterPoint = getListCenterPoint();
    tempListPoints.clear();
    iMoveRev = lstPoints.rbegin();
    tempListPoints.push_front(*iMoveRev);
    //iMoveRev = lstPoints.rend();
    ++iMoveRev;
    tempListPoints.push_front(*iMoveRev);
    tempListPoints.push_front(CenterPoint);

    checkCCW = IsOnCCWOrder(tempListPoints);

    if (!checkCCW)
    {
        lstPoints.reverse();
        /*
        for (iMoveRev = lstPoints.rbegin(); iMoveRev != lstPoints.rend(); ++iMoveRev)
            AuxList.push_back(*iMoveRev);

        lstPoints = AuxList;
        */
    }

    vecAux.clear();
    for (iMove = lstPoints.begin(); iMove != lstPoints.end(); ++iMove)
        vecAux.push_back(*iMove);

    for (i = 1; i < vecAux.size(); i++)
    {
        tempListPoints.clear();
        tempListPoints.push_back(vecAux[i-1]);
        tempListPoints.push_back(vecAux[i]);
        tempListPoints.push_back(CenterPoint);
        if (!IsOnCCWOrder(tempListPoints))
        {
            tempPoint = vecAux[i];
            vecAux[i] = vecAux[i-1];
            vecAux[i-1] = tempPoint;
            i = 0;
        }
    }

    lstPoints.clear();
    for (i = 0; i < vecAux.size(); i++)
        lstPoints.push_back(vecAux[i]);
};
//---------------------------------------------------------------------
bool CPath::PointIsOnList(v3 vTest)
{
    bool retValue = false;
    v3 currV3;
    list<v3>::iterator itMove, itFim;

    if (lstPoints.size() < 1)
        return retValue;

    itMove = lstPoints.begin();
    itFim  = lstPoints.end();
    while(itMove != itFim)
    {
        currV3 = *itMove;
        if (vTest == currV3)
        {
            retValue = true;
            break;
        }
        ++itMove;
    }
    return retValue;
};
//---------------------------------------------------------------------
bool CPath::PointIsOnPool(pointList vTest)
{
    unsigned int i, nPoolSize, nTestSize;
    bool retValue = false;
    PointGroup tempPointList;
    v3 currV3;

    nTestSize = vTest.size();
    if ( (vecPointsOnZPath.size() < 1) || (nTestSize < 1))
        return retValue;

    nPoolSize = vecPointsOnZPath.size();

    i = 0;
    while ((i < nPoolSize) && (retValue == false) )
    {
        //cout << "i: " << i << "(" << nPoolSize << ")" << endl;
        //tempPointList = this->vecPointsOnPath[i];
        tempPointList = vecPointsOnZPath[i];
        retValue = ComparePointList(tempPointList.vecPoints,vTest);
/*
        nPointsTemp = tempPointList.vecPoints.size();
        cout << "COMAPRANDO PONTOS: " << nPointsTemp << endl;
        for (j = 0; j < nPointsTemp; j++)
        {
            currV3 = tempPointList.vecPoints[j];
            for (k = 0; k < vTest.size(); k++)
                if (vTest[k] == currV3)
                {
                    retValue = true;
                    break;
                }
        }
*/
        i++;
    };
    return retValue;
};
//---------------------------------------------------------------------
void CPath::PrintList(ofstream &Filestream)
{
    unsigned int i, j, nListPoints, nPoints, nIndex;
    PointGroup tempPoints;
    indexList tempIndex;
    nListPoints = vecPointsOnZPath.size();
    if (nListPoints == 0)
        return;

    for (i = 0 ; i < nListPoints; i++)
    {
        tempPoints = vecPointsOnZPath[i];
        nPoints = tempPoints.vecPoints.size();
        nIndex = tempPoints.AdjIndex.size();

        Filestream << tempPoints.triIndex;
        for (j = 0; j < nPoints; j ++)
            Filestream << "\t" << tempPoints.vecPoints[j];
        for (j = 0; j < nIndex; j ++)
            Filestream << "\t" << tempPoints.AdjIndex[j];
        Filestream << endl;

    }
};
//---------------------------------------------------------------------
bool CPath::pointInClosePathList(v3 pTest)
{
    int   i, j, nListPoints;
    bool  oddNodes = false;


    vector<v3> poly;
    list<v3>::iterator iMove;


    for (iMove = lstPoints.begin(); iMove != lstPoints.end(); ++iMove)
        poly.push_back(*iMove);

    nListPoints = (int)poly.size();
    j = nListPoints-1;
    for (i = 0; i < nListPoints; i++)
    {
        if ( ( ( (poly[i].m_y < pTest.m_y) && (poly[j].m_y >= pTest.m_y) ) ||
               ( (poly[j].m_y < pTest.m_y) && (poly[i].m_y >= pTest.m_y) ) ) &&
             ( (poly[i].m_x <= pTest.m_x) || (poly[j].m_x <= pTest.m_x) ) )
        {
            oddNodes ^= (poly[i].m_x + (pTest.m_y - poly[i].m_y)/(poly[j].m_y-poly[i].m_y)*(poly[j].m_x-poly[i].m_x)< pTest.m_x);
        }
        j = i;
    }
    return oddNodes;
};
//=====================================================================
//---------------------------------------------------------------------
unsigned int CSTLPathGroup::PointNumber()
{
    unsigned int i, nPaths, NumPoints;
    NumPoints = 0;
    nPaths = vecPaths.size();
    for (i = 0; i < nPaths; i++)
        NumPoints += this->vecPaths[i].lstPoints.size();
    return NumPoints;
};
//---------------------------------------------------------------------
int CSTLPathGroup::SetBorderAndHole()
{
    unsigned int i, j, nPaths;
    CPath tempPathI, tempPathJ;
    double maxIpx, maxIpy, minIpx, minIpy, maxJpx, maxJpy, minJpx, minJpy, AreaI, AreaJ;
    bool b_I_Inside_J, itisHole;
    //list<v3> tempList;
    //v3 tempV3;

    list<v3>::iterator iMove;
    nPaths = vecPaths.size();
    for(i = 0; i < nPaths; i++)
    {
        tempPathI = vecPaths[i];

        maxIpx = tempPathI.getMaxCoord(0);
        maxIpy = tempPathI.getMaxCoord(1);
        minIpx = tempPathI.getMinCoord(0);
        minIpy = tempPathI.getMinCoord(1);
        AreaI = (maxIpx - minIpx)*(maxIpy - minIpy);
        for(j = 0; j < nPaths; j++)
        {
            if (i == j)
                continue;


            tempPathJ = vecPaths[j];

            maxJpx = tempPathJ.getMaxCoord(0);
            maxJpy = tempPathJ.getMaxCoord(1);
            minJpx = tempPathJ.getMinCoord(0);
            minJpy = tempPathJ.getMinCoord(1);
            AreaJ = (maxJpx - minJpx)*(maxJpy - minJpy);

            b_I_Inside_J = (maxIpx < maxJpx) && (maxIpy < maxJpy) &&
                           (minIpx > minJpx) && (minIpy > minJpy);
            if ( (AreaI > AreaJ) || (!b_I_Inside_J) )
                continue;
            else
            {
                //teste se cada ponto de J está dentro do caminhio I
                //tempList = tempPathJ.lstPoints;
                for (iMove = tempPathI.lstPoints.begin(); iMove != tempPathI.lstPoints.end(); ++iMove)
                {
                    //tempV3 = *iMove;
                    itisHole  = tempPathJ.pointInClosePathList(*iMove);

                }

            }
            if (itisHole)
                vecPaths[i].isHole = itisHole;
        }
    }
    return 0;
};
//---------------------------------------------------------------------
