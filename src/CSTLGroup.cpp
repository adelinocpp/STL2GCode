#include "CSTLGroup.h"

using namespace std;

CSTLGroup::CSTLGroup()
{
    vecAdjTriGroup.clear();
};
//---------------------------------------------------------------------
// Separa os trianglos em peças e constroi a lista de triangulos adjacentes
int CSTLGroup::Process_Triangles(list<tri> vecT)
{
    unsigned int i, j, nTriangles, numTriangles;
    CAdjacentsTriangles tempAdjTriangles;
    vector<int> vecTriangleIndex;
    tri tempVebTri;
    numTriangles = vecT.size();

    for (i = 0; i < numTriangles; i++)
        vecTriangleIndex.push_back(i);

    vecAdjTriGroup.clear();
    do
    {
        tempAdjTriangles = CAdjacentsTriangles();
        numTriangles = tempAdjTriangles.AddAdjacent(vecT,vecTriangleIndex);
        nTriangles = vecT.size();

        if (numTriangles > 0)
        {
            tempAdjTriangles.BuilAdjacentList();
            vecAdjTriGroup.push_back(tempAdjTriangles);
        }


    } while( (numTriangles != 0) && (nTriangles > 0));

    return (int)vecAdjTriGroup.size();
};
//---------------------------------------------------------------------
int CSTLGroup::Count_Triangles()
{
    unsigned int i, numGroups;
    unsigned int CoutTri = 0;
    numGroups = vecAdjTriGroup.size();
    CAdjacentsTriangles tempAdjTriangles;
    for(i = 0; i < numGroups; i++)
    {
        tempAdjTriangles = vecAdjTriGroup[i];
        CoutTri += tempAdjTriangles.vecTriangles.size();
    }
    return CoutTri;
};
//---------------------------------------------------------------------
int CSTLGroup::ProjectZ(double z)
{
    unsigned int i, j, nGroups, nPaths;
    v3 tempPoint0, tempPoint1, projPoint;

    tri tempTriangle;
    CAdjacentsTriangles tempPiece;
    CSTLPathGroup retSTLGroup, tempGroupPath;
    CPath tempPath;

    v3 vecL1, vecL2, PointZ(1,1,0);
    vector<tri> vecOut;

    vector<v3> listOfPoints;
    listOfPoints.clear();

    nGroups = vecAdjTriGroup.size();

    vecGroupPath.clear();
    for (i = 0; i < nGroups; i++) // Loop nos grupos de peças
    {
        tempPiece = vecAdjTriGroup[i];

        tempPiece.AnaliseProjectZ(z);
        tempPiece.BuildLinksProjectZ();
        nPaths = tempPiece.ProcessPaths();
        vecGroupPath.push_back(tempPiece.grupoCaminhos);
        cout << "Numero de caminhos(**): " << nPaths << endl;
    }

    nGroups = vecGroupPath.size();

    if (nGroups < 1)
        return 0;

    for (i = 0; i < nGroups; i++)
    {
        tempGroupPath = vecGroupPath[i];
        nPaths = tempGroupPath.vecPaths.size();
        for(j = 0; j < nPaths; j++)
        {
            tempPath = tempGroupPath.vecPaths[j];
            tempPath.MakeCircularOrder();
            tempPath.DownToPointList();
            tempPath.AjustCCWList();
            //tempPath.AjustCCWList();
            tempGroupPath.vecPaths[j] = tempPath;
        }
        tempGroupPath.SetBorderAndHole();
        vecGroupPath[i] = tempGroupPath;
    }
    return (int)vecGroupPath.size();
};
//---------------------------------------------------------------------
void CSTLGroup::PrintPathGroup(string Filename)
{
    unsigned int i, j, nGroup, nPath, nList;
    CPath tempPath;
    CSTLPathGroup tempGroupPath;
    list<v3> lstOfPoints;
    list<v3>::iterator nMove;
    v3 tempPoint;
    nGroup = vecGroupPath.size();

    if (nGroup < 1)
        return;

    ofstream TriFile (Filename.c_str());

    for (i = 0; i < nGroup; i++)
    {
        tempGroupPath = vecGroupPath[i];
        nPath = tempGroupPath.vecPaths.size();
        for(j = 0; j < nPath; j++)
        {
            tempPath = tempGroupPath.vecPaths[j];
            lstOfPoints = tempPath.lstPoints;
            for (nMove = lstOfPoints.begin(); nMove != lstOfPoints.end(); ++nMove)
            {
                //tempPoint = *nMove;
                TriFile << *nMove << endl;
            }
            TriFile << v3(-1,-1,(int)tempPath.isHole) <<  endl;
        }
    }
    TriFile.close();
};
//---------------------------------------------------------------------
void CSTLGroup::PrintRawPathGroup(string Filename)
{
    unsigned int i, j, nGroup, nPath;
    CPath tempPath;
    CSTLPathGroup tempGroupPath;
    list<v3> lstOfPoints;
    list<v3>::iterator nMove;
    v3 tempPoint;
    nGroup = vecGroupPath.size();

    if (nGroup < 1)
        return;

    ofstream TriFile (Filename.c_str());

    for (i = 0; i < nGroup; i++)
    {
        tempGroupPath = vecGroupPath[i];
        nPath = tempGroupPath.vecPaths.size();
        for(j = 0; j < nPath; j++)
        {
            tempPath = tempGroupPath.vecPaths[j];
            tempPath.PrintList(TriFile);
            TriFile << "-------------" << endl;
        }
    }
    TriFile.close();
};
//---------------------------------------------------------------------
void CSTLGroup::PrintIdxTriangleListAdj(string Filename)
{
    unsigned int i, j, k, nGroup, nTriangles, nList;
    CAdjacentsTriangles tempGroupTriangles;
    indexList  tempListaAdj;
    tri tempTriangle;
    list<v3> lstOfPoints;
    list<v3>::iterator nMove;
    v3 tempPoint;
    nGroup = vecAdjTriGroup.size();

    if (nGroup < 1)
        return;

    ofstream TriFile (Filename.c_str());
    for (i = 0; i < nGroup; i++)
    {
        tempGroupTriangles = vecAdjTriGroup[i]; // CAdjacentsTriangles
        nTriangles = tempGroupTriangles.vecTriangles.size();
        for (k = 0; k < nTriangles; k++)
        {
            tempTriangle = tempGroupTriangles.vecTriangles[k];
            TriFile << "i: " << tempTriangle.triIndex << "\t";
            for(j = 0; j < 3; j++)
            {
                TriFile << tempTriangle.m_p[j] << "\t";
            }
            tempListaAdj = tempTriangle.AdjIndex;
            nList = tempListaAdj.size();
            for(j = 0; j < nList; j++)
            {
                TriFile << tempListaAdj[j] << "\t";
            }
            TriFile << endl;
        }
        TriFile << "----------------------------------------------------------------------" << endl;
    }
    TriFile.close();
};
//---------------------------------------------------------------------
bool CSTLGroup::BuildInitialRoute()
{
    unsigned int i, j, nGroups, nPaths;

    nGroups = vecGroupPath.size();

    for (i = 0; i < nGroups; i++)
    {

    }


};
