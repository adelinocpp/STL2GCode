#include "v3.h"

#define Abs(x)    ((x) <= 0 ? -(x) : (x))
#define Max(a, b) ((a) >= (b) ? (a) : (b))

double RelDif(double a, double b)
{
	double c = Abs(a);
	double d = Abs(b);

	d = Max(c, d);

	return d == 0.0 ? 0.0 : Abs(a - b) / d;
};
//---------------------------------------------------------------------
v3::v3(double x, double y, double z)
{
    m_x = x;
    m_y = y;
    m_z = z;
    epi_D = 0.00001;
};
//---------------------------------------------------------------------
v3::v3(char* facet)
{
    char f1[4] = {facet[0],
        facet[1],facet[2],facet[3]};

    char f2[4] = {facet[4],
        facet[5],facet[6],facet[7]};

    char f3[4] = {facet[8],
        facet[9],facet[10],facet[11]};

    float xx = *((float*) f1 );
    float yy = *((float*) f2 );
    float zz = *((float*) f3 );

    m_x = double(xx);
    m_y = double(yy);
    m_z = double(zz);
    epi_D = 0.00001;
};
//---------------------------------------------------------------------
double v3::mod()
{
    return sqrt(m_x*m_x + m_y*m_y + m_z*m_z);
};
//---------------------------------------------------------------------
double v3::angle()
{
    return acos(-m_x/this->mod());
};
//---------------------------------------------------------------------
double v3::AngleWith(v3 other)
{
    v3 temp0;
    double dCosAngle;
    dCosAngle = dot_prod(other)/(mod()*other.mod());
    return acos(dCosAngle);
};
//---------------------------------------------------------------------
double v3::dist(const v3 other)
{
    v3 temp0, temp1;
    temp0 = *this;
    temp1 = temp0-other;
    return temp1.mod();
};
//---------------------------------------------------------------------
double v3::dot_prod(const v3 other)
{
    return ((m_x*other.m_x) + (m_y*other.m_y) + (m_z*other.m_z) );
};
//---------------------------------------------------------------------
v3 v3::cross_prod(const v3 other)
{
    v3 tv3;
    tv3.m_x = this->m_y*other.m_z - this->m_z*other.m_y;
    tv3.m_y = this->m_z*other.m_x - this->m_x*other.m_z;
    tv3.m_z = this->m_x*other.m_y - this->m_y*other.m_x;
    return tv3;
};
//---------------------------------------------------------------------
v3 v3::operator*(const double scalar)
{
    v3 tv3;
    tv3.m_x = m_x*scalar;
    tv3.m_y = m_y*scalar;
    tv3.m_z = m_z*scalar;
    return tv3;
};
//---------------------------------------------------------------------
v3 v3::operator/(const double scalar)
{
    v3 tv3;
    tv3.m_x = m_x/scalar;
    tv3.m_y = m_y/scalar;
    tv3.m_z = m_z/scalar;
    return tv3;
};
//---------------------------------------------------------------------
v3 v3::operator-(const v3 other)
{
    v3 tv3;
    tv3.m_x = m_x - other.m_x;
    tv3.m_y = m_y - other.m_y;
    tv3.m_z = m_z - other.m_z;
    return tv3;
};
//---------------------------------------------------------------------
v3 v3::operator+(const v3 other)
{
    v3 tv3;
    tv3.m_x = m_x + other.m_x;
    tv3.m_y = m_y + other.m_y;
    tv3.m_z = m_z + other.m_z;
    return tv3;
};
//---------------------------------------------------------------------
bool v3::operator==(const v3 other)
{
  return ( ( (RelDif(m_x,other.m_x) <= epi_D) && (RelDif(m_y,other.m_y) <= epi_D) )
           && (RelDif(m_z,other.m_z) <= epi_D)  );
  //return ((m_x == other.m_x) && (m_y == other.m_y) && (m_z == other.m_z) );
}
//---------------------------------------------------------------------
bool v3::operator!=(const v3 other)
{
    return ( (RelDif(m_x,other.m_x) >= epi_D) ||
             (RelDif(m_y,other.m_y) >= epi_D) ||
             (RelDif(m_z,other.m_z) >= epi_D));
    //return ((m_x != other.m_x) || (m_y != other.m_y) || (m_z != other.m_z) );
};
//---------------------------------------------------------------------
bool v3::operator<(const v3 other)
{
    /*
    if (m_z != other.m_z)
        return m_z < other.m_z;
    else
    {
        if (m_y != other.m_y)
            return m_y < other.m_y;
        return m_x < other.m_x;
    }
    return 0;
    */

    double R1,R2;
    R1 = sqrt(m_x*m_x + m_y*m_y + m_z*m_z);
    R2 = sqrt(other.m_x*other.m_x + other.m_y*other.m_y + other.m_z*other.m_z);
    return R1 < R2;
};
//---------------------------------------------------------------------
bool v3::operator>(const v3 other)
{
    if (m_z != other.m_z)
        return m_z > other.m_z;
    else
    {
        if (m_y != other.m_y)
            return m_y > other.m_y;
        return m_x > other.m_x;
    }
/*
    double R1,R2;
    R1 = sqrt(m_x*m_x + m_y*m_y + m_z*m_z);
    R2 = sqrt(other.m_x*other.m_x + other.m_y*other.m_y + other.m_z*other.m_z);
    return R1 > R2;
    */
};
//---------------------------------------------------------------------
ostream& operator<<(ostream& os, const v3& v)
{
    //os << "(" << v.m_x << "," << v.m_y << "," << v.m_z << ") ";
    os << v.m_x << "\t" << v.m_y << "\t" << v.m_z;
    return os;
};
//---------------------------------------------------------------------
