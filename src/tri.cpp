#include "tri.h"

//---------------------------------------------------------------------
int tri::NumCompVertex(tri triTest)
{
    unsigned int i, j;
    int numVertex;

    numVertex = 0;
    for(i = 0; i < 3; i++)
    {
        for(j = 0; j < 3; j++)
        {
            if ( (triTest.m_p[j].m_x == m_p[i].m_x) &&
                 (triTest.m_p[j].m_y == m_p[i].m_y) &&
                 (triTest.m_p[j].m_z == m_p[i].m_z) )
            {
                numVertex++;
            }
        }
    }
    return numVertex;
};
//---------------------------------------------------------------------
tri::tri(v3 p1, v3 p2, v3 p3)
{
    m_p[0] = p1;
    m_p[1] = p2;
    m_p[2] = p3;
    u_v = v3(0,0,0);
};
//---------------------------------------------------------------------
tri::tri(v3 p1, v3 p2, v3 p3, v3 uv)
{
    m_p[0] = p1;
    m_p[1] = p2;
    m_p[2] = p3;
    u_v = uv;
};
//---------------------------------------------------------------------
