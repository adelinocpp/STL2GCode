#include "STLfunctions.h"

//=====================================================================
list<tri> AlingWith(vector<tri> v, v3 p1)
{
    unsigned int i, j, nTriangles;
    v3 maxPoint, minPoint, tempPoint;
    tri tempTriangle;

    list<tri> retList;
    retList.clear();

    maxPoint = v3(DBL_MIN,DBL_MIN,DBL_MIN);
    minPoint = v3(DBL_MAX,DBL_MAX,DBL_MAX);
    nTriangles = v.size();

    for (i = 0; i < nTriangles; i++)
    {
        tempTriangle = v[i];
        for (j = 0; j < 3; j++)
        {
            tempPoint = tempTriangle.m_p[j];
            if (maxPoint.m_x < tempPoint.m_x)
                maxPoint.m_x = tempPoint.m_x;
            if (maxPoint.m_y < tempPoint.m_y)
                maxPoint.m_y = tempPoint.m_y;
            if (maxPoint.m_z < tempPoint.m_z)
                maxPoint.m_z = tempPoint.m_z;
            if (minPoint.m_x > tempPoint.m_x)
                minPoint.m_x = tempPoint.m_x;
            if (minPoint.m_y > tempPoint.m_y)
                minPoint.m_y = tempPoint.m_y;
            if (minPoint.m_z > tempPoint.m_z)
                minPoint.m_z = tempPoint.m_z;

        }
    }
    for (i = 0; i < nTriangles; i++)
    {
        tempTriangle = v[i];
        for (j = 0; j < 3; j++)
        {
            tempPoint = tempTriangle.m_p[j];
            tempPoint.m_x -= minPoint.m_x + p1.m_x;
            tempPoint.m_y -= minPoint.m_y + p1.m_y;
            tempPoint.m_z -= maxPoint.m_z + p1.m_z;
            tempTriangle.m_p[j] = tempPoint;
        }
        retList.push_back(tempTriangle);
    }
    return retList;
};
//=====================================================================
bool IndexInsideVector(vector<int> vecI, int testIndex)
{
    unsigned int i, nSize;
    bool retValue = false;
    nSize = vecI.size();
    if (nSize < 0)
        return retValue;
    for(i = 0; i < nSize; i++)
        retValue = retValue || (vecI[i] == testIndex);
    return retValue;
};
/*
vector<v3> ProjectZ(vector <tri>&v, double z)
{
    unsigned int i, j, nTriangles, idx0, idx1;
    v3 tempPoint0, tempPoint1, projPoint;
    double tFactor;
    tri tempTriangle;
    vector<v3> vecOut;

    nTriangles = v.size();
    for (i = 0; i < nTriangles; i++)
    {
        tempTriangle = v[i];
        for (j = 0; j < 3; j++)
        {
            idx0 = j;
            idx1 = (j+1) % 3;

            tempPoint0 = tempTriangle.m_p[idx0];
            tempPoint1 = tempTriangle.m_p[idx1];
            if ((tempPoint0.m_z - z)*(tempPoint1.m_z - z) < 0)
            {
                tFactor = (tempPoint1.m_z-tempPoint0.m_z)/z;
                projPoint.m_x = (tempPoint1.m_x-tempPoint0.m_x)*tFactor + tempPoint0.m_x;
                projPoint.m_y = (tempPoint1.m_y-tempPoint0.m_y)*tFactor + tempPoint0.m_y;
                projPoint.m_z = z - z;
                vecOut.push_back(projPoint);
            }

        }

    }
    return vecOut;

};
*/
//=====================================================================
/*
vector<Path> find_path(vector<v3> vPoint)
{
    unsigned int i, j, nVertex, nPath, idx0, idx1;
    double Path_length;
    Path tempPath;
    vector<Path> vecPath;
    v3 VertexI, VertexJ;
    nVertex = vPoint.size();
    vector<int> vecI, vecJ;


    for(i = 0; i < (nVertex-1); i++)
    {
        VertexI = vPoint[i];
        for(j = i+1; j < nVertex; j++)
        {
            VertexJ = vPoint[j];
            if( (VertexI.m_x == VertexJ.m_x)  && (VertexI.m_y == VertexJ.m_y) && (VertexI.m_z == VertexJ.m_z) )
            {
                vecI.push_back(i);
                vecJ.push_back(j);
            }

        }
    }

    nPath = vecI.size();
    for(i = 0; i < nPath; i++)
    {
        idx0 = vecI[i];
        idx1 = vecJ[i];
        tempPath = Path();
        Path_length = 0;
        for (j = idx0; j < idx1; j++)
        {
            tempPath.vecPoints.push_back(vPoint[j]);
            Path_length = sqrt( pow(vPoint[j].m_x - vPoint[j+1].m_x,2) +
                                pow(vPoint[j].m_y - vPoint[j+1].m_y,2) +
                                pow(vPoint[j].m_z - vPoint[j+1].m_z,2) );
        }
        if (Path_length > 0)
        {
            tempPath.close_path =  ( (vPoint[idx0].m_x == vPoint[idx1].m_x) &&
                                     (vPoint[idx0].m_y == vPoint[idx1].m_y) &&
                                     (vPoint[idx0].m_z == vPoint[idx1].m_z) );
            vecPath.push_back(tempPath);
        }
    }

    return vecPath;
}
*/
//=====================================================================

//=====================================================================
int read_stl(string fname, vector<tri> &v)
{
    //!!
    //don't forget ios::binary
    //!!
    ifstream myFile (fname.c_str(), ios::in | ios::binary);
    //ifstream myFile (fname.c_str(), ios::binary);

    char header_info[80] = "";
    char nTri[4];
    unsigned long nTriLong, i;

    cout << fname.c_str() << endl;
    //read 80 byte header
    if (myFile.is_open())
    {
        myFile.read (header_info, 80);
        cout <<"header: " << header_info << endl;
    }
    else
    {
        cout << "error" << endl;
        return -1;
    }

    //read 4-byte ulong
    if (myFile)
    {
        myFile.read(nTri, 4);
        //nTriLong = *((unsigned long*)nTri) ;
        nTriLong = *((unsigned int32_t*)nTri);
        cout <<"n Tri: " << nTriLong << endl;
    }
    else{
        cout << "error" << endl;
        return -1;
    }

    //now read in all the triangles
    for(i = 0; i < nTriLong; i++){

        char facet[50];

        if (myFile) {

        //read one 50-byte triangle
            myFile.read (facet, 50);

        //populate each point of the triangle
        //using v3::v3(char* bin);
            //facet + 12 skips the triangle's unit normal
            v3 uv(facet);
            v3 p1(facet+12);
            v3 p2(facet+24);
            v3 p3(facet+36);

            //add a new triangle to the array
            v.push_back( tri(p1,p2,p3,uv) );

        }
    }
    return 0;
}
//---------------------------------------------------------------------

//--------------------------------------------------------------------------
void swap(double &a, double &b)
{
    double tmp;
    tmp = a;
    a = b;
    b = tmp;
}
//---------------------------------------------------------------------
void swap(int &a, int &b)
{
    int tmp;
    tmp = a;
    a = b;
    b = tmp;
}
//---------------------------------------------------------------------
int partition(vector<double> &vec, vector<int> &idx, int left, int right)
{
    int i, j;

    i = left;
    for (j = left + 1; j <= right; ++j)
    {
        if (vec[j] < vec[left])
        {
            ++i;
            swap(vec[i], vec[j]);
            swap(idx[i], idx[j]);
        }
    }
    swap(vec[left], vec[i]);
    swap(idx[left], idx[i]);

    return i;
}
//---------------------------------------------------------------------
void quickSort(vector<double> &vec, vector<int> &idx, int left, int right)
{
    int r;
    if (right > left)
    {
        r = partition(vec, idx, left, right);
        quickSort(vec, idx, left, r - 1);
        quickSort(vec, idx, r + 1, right);
    }
}
//---------------------------------------------------------------------
void SortVector(vector<double> &vec, vector<int> &idx, int dec)
{
    // dec = 0 -> acend; dec = 1 -> descend
    int i;
    vector<double> tVec;
    vector<int> tIdx;

    if(idx.size() < 2)
    {
        idx.clear();
        for(i = 0; i < (int)vec.size(); i++)
            idx.push_back(i);
    }

    quickSort(vec, idx, 0, ((int)vec.size() - 1));
    if(dec == 1)
    {
        tVec = vec;
        tIdx = idx;
        idx.clear();
        vec.clear();
        for(i = ((int)tVec.size()-1); i >=0; i--)
        {
            vec.push_back(tVec[i]);
            idx.push_back(tIdx[i]);
        }
    }
};
//---------------------------------------------------------------------
void SortByVector(vector<v3> &vec, vector<int> idx, int dec)
{
    // dec = 0 -> acend; dec = 1 -> descend
    int i;
    vector<v3> tVec;
    tVec = vec;
    vec.clear();

    if(dec == 0)
        for(i = 0; i < (int)tVec.size(); i++)
            vec.push_back(tVec[idx[i]]);
    else
        for(i = ((int)tVec.size() -1); i >= 0; i--)
            vec.push_back(tVec[idx[i]]);
};
//---------------------------------------------------------------------
//=====================================================================
