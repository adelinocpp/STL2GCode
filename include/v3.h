#ifndef V3_H
#define V3_H

#include <ostream>
#include <math.h>
#include <stack>

using namespace std;

class v3
{
    public:
        inline v3(){epi_D = 0.00001;};
        v3(char* bin);
        v3(double x, double y, double z);
        virtual ~v3() {};

        double AngleWith(const v3 other);
        double dot_prod(v3 other);
        double mod();
        double angle();
        double dist(const v3 other);
        v3 cross_prod(const v3 other);
        v3 operator*(const double scalar);
        v3 operator/(const double scalar);
        v3 operator-(const v3 other);
        v3 operator+(const v3 other);
        bool operator==(const v3 other);
        bool operator!=(const v3 other);
        bool operator<(const v3 other);
        bool operator>(const v3 other);

        friend ostream& operator<<(ostream& os, const v3& v);

        // counterclockwise (anti-horário)

        int pointIndex;
        double m_x, m_y, m_z;
        double epi_D;
    protected:
    private:
};

#endif // V3_H
