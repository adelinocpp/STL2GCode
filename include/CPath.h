#ifndef CPATH_H
#define CPATH_H

#include<iostream>
#include <vector>
#include <list>
#include <algorithm>
#include "v3.h"
#include "STLfunctions.h"

using namespace std;
using namespace math;

v3 MeanPoint(pointList p1);
bool IsOnCCWOrder(pointList &pL1);
bool ComparePointList(pointList p1, pointList p2);
void AdjustCCWOrderWithPoint(pointList &pL1, v3 p3);
void AdjustCCWOrderNearPoint(pointList &pL1, v3 p4);
//---------------------------------------------------------------------
class CPath
{
    public:
        inline CPath() {lstPoints.clear(); vecPointsOnZPath.clear(); isHole = false;};
        virtual ~CPath(){};
        void OffSet(v3 vOS);

        v3 getMeanPoint();
        v3 getGroupCenterPoint();
        v3 getListCenterPoint();
        v3 getMaxPoint();
        v3 getMinPoint();
        double getMaxCoord(int coordN);
        double getMinCoord(int coordN);
        bool PointIsOnList(v3 vTest);
        bool PointIsOnPool(pointList vTest);
        PointGroup PointGroupOnPoolByIndex(int idxPoint);
        vector<double> AngleWithCenterOfPointGroupOnPoolByIndex(int idxPoint);
        //bool PointOnPoolByIndex(int idxPoint);
        void MakeCircularOrder();
        void DownToPointList();
        void RemoveRepeted();
        void AjustCCWList();
        bool pointInClosePathList(v3 pTest);


        // TODO: Escrever estas funções e ordenar vecPointsOnZPath
        bool sortVecPointsOnZPathByDist();
        bool sortVecPointsOnZPathByAngle();


        list<v3> lstPoints;
        list<int> lstIndexPoint;
        bool isHole;

        vector<PointGroup> vecPointsOnZPath;

        void PrintList(ofstream &Filestream);
    protected:

    private:
};

//---------------------------------------------------------------------
class CSTLPathGroup
{
    public:
        inline CSTLPathGroup() {vecPaths.clear();};
        virtual ~CSTLPathGroup(){};
        unsigned int PointNumber();
        int SetBorderAndHole();
        vector<CPath> vecPaths;
    protected:

    private:
};
//---------------------------------------------------------------------

#endif // CPATH_H
