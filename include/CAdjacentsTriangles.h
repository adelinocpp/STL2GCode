#ifndef CADJACENTSTRIANGLES_H
#define CADJACENTSTRIANGLES_H

#include <iostream>
#include <vector>
#include <list>
#include "tri.h"
#include "CPath.h"


using namespace std;

class CAdjacentsTriangles
{
    public:
        inline CAdjacentsTriangles() {vecTriangles.clear(); Zprojection = false; Zplane = 0;};
        virtual ~CAdjacentsTriangles() {};
        int AddAdjacent(list<tri> &vecT, vector<int> &vOriIndex);
        bool BuilAdjacentList();
        int numConectionsAdjacentList();
        bool AnaliseProjectZ(double z);
        bool BuildLinksProjectZ();
        int getNumPointsOnZplane();
        int ProcessPaths();
        int GetNumPaths();

        // Processamento da imagem STL
        vector<tri> vecTriangles;

        // Processamento da projecao
        vector<PointGroup> vecPointsOnZplane;

        CSTLPathGroup grupoCaminhos;

        double Zplane;
        bool Zprojection;
    protected:

    private:
};

#endif // CADJACENTSTRIANGLES_H
