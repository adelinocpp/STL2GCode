#ifndef STLFUNCTIONS_H_INCLUDED
#define STLFUNCTIONS_H_INCLUDED

#include <iostream>
#include <fstream>
#include <float.h>
#include <string>
#include <list>
#include <vector>
#include "tri.h"
#include "v3.h"
#include "matrix.h"

using namespace std;
//---------------------------------------------------------------------
//typedef vector<tri> vecTriangle;

//---------------------------------------------------------------------
list<tri> AlingWith(vector<tri> v, v3 p1);
//vector<v3> ProjectZ(vector <tri>&v, double z);
//vector<Path> find_path(vector<v3> vPoint);
bool IndexInsideVector(vector<int> vecI, int testIndex);

int read_stl(string fname, vector<tri> &v);

void swap(double &a, double &b);
void swap(int &a, int &b);
int partition(vector<double> &vec, vector<int> &idx, int left, int right);
void quickSort(vector<double> &vec, vector<int> &idx, int left, int right);
void SortVector(vector<double> &vec, vector<int> &idx, int dec = 0);
void SortByVector(vector<v3> &vec, vector<int> idx, int dec = 0);

//---------------------------------------------------------------------
#endif // STLFUNCTIONS_H_INCLUDED
