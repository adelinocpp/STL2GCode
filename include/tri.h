#ifndef TRI_H
#define TRI_H

#include <vector>
#include <deque>
#include "v3.h"

typedef deque<int> indexList;
typedef deque<v3> pointList;

//---------------------------------------------------------------------
class tri
{
    public:
        inline tri(){};
        tri(v3 p1, v3 p2, v3 p3);
        tri(v3 p1, v3 p2, v3 p3, v3 uv);
        int NumCompVertex(tri triTest);
        void draw();
        virtual ~tri() {};

        v3 m_p[3];
        v3 u_v;
        int triIndex;
        indexList AdjIndex;
    protected:

    private:
};
//---------------------------------------------------------------------
class PointGroup
{
    public:
        inline PointGroup() {triIndex = -1; vecPoints.clear(); AdjIndex.clear();};
        virtual ~PointGroup() {};

        int triIndex;
        pointList vecPoints;
        indexList AdjIndex;
    protected:

    private:
};

#endif // TRI_H
