#ifndef CSTLGROUP_H
#define CSTLGROUP_H

#include <iostream>
#include <math.h>
#include "STLfunctions.h"
#include "CAdjacentsTriangles.h"
#include "CPath.h"

using namespace std;

class CSTLGroup
{
    public:
        CSTLGroup();
        virtual ~CSTLGroup() {};

        int Process_Triangles(list<tri> vecT);
        int Count_Triangles();
        int ProjectZ(double z);
        void PrintPathGroup(string Filename);
        void PrintRawPathGroup(string Filename);
        void PrintIdxTriangleListAdj(string Filename);
        bool BuildInitialRoute();
        //int ProcessAdjacentTriangles(vector<CAdjacentsTriangles> vecAdjTri);

        vector<CAdjacentsTriangles> vecAdjTriGroup;
        vector<CSTLPathGroup> vecGroupPath;

    protected:


    private:
};

#endif // CSTLGROUP_H
