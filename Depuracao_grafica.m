close all;
clearvars;
clc;

M = dlmread('pontoZ.txt','\t');
X=M(1:(end-1),1);
Y=M(1:(end-1),2);
% 
% figure, plot(X,Y,'o')
% figure, plot(X,Y,'-.')

M = [[-1,-1,-1]; M];
X=M(:,1);
Y=M(:,2);
idx00 = find((X == -1) & (Y == -1));
figure
for i = 1:(length(idx00)-1)
    idx1 = idx00(i)+1;
    idx2 = idx00(i+1)-1;
    hold on, plot(X(idx1:idx2),Y(idx1:idx2))
    hold on, plot(X(idx1),Y(idx1),'kx')
    hold on, plot(X(idx2),Y(idx2),'ko')
end

return,
M = dlmread('Triangulos.csv','\t');
Xt = M(:,1);
Yt = M(:,2);
Zt = M(:,3);
figure,
for i = 1:3:length(Xt)
    hold on, plot3(Xt(i:i+2),Yt(i:i+2),Zt(i:i+2));
end